import first.A_Measurement;
import second.B_Measurement;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        System.out.println("Implementation of Dijkstra Algorithm, Raffaele Sassano, Algorithm Engineering, WS 2017/18\n");

        executePrimitiveImplementation_gen();
        executeOptimizedImplementation_gen();
        executeOptimizedImplementation_real();
    }

    private static void executePrimitiveImplementation_gen() {
        int[] numberOfNodes = {20, 40, 60, 80, 100, 120, 140, 160, 180, 200};
        String startNodeId = "0";
        int numberOfMeasurements = 20;

        for (Integer i : numberOfNodes) {
            A_Measurement a_measurement = new A_Measurement(i, startNodeId, numberOfMeasurements);
            System.out.println(a_measurement.getAverageMeasurement());
        }
    }

    private static void executeOptimizedImplementation_real() {
        String filePath = "./data/usa/USA-road-d.NY.gr";
        String startNodeId = "1";
        int numberOfMeasurements = 10;
        B_Measurement a_measurement = new B_Measurement(filePath, startNodeId, numberOfMeasurements);
        System.out.println(a_measurement.getAverageMeasurement());
    }

    private static void executeOptimizedImplementation_gen() {
        int[] numberOfNodes = {20, 40, 60, 80, 100, 120, 140, 160, 180, 200};
        String startNodeId = "0";
        int numberOfMeasurements = 20;

        for (Integer i : numberOfNodes) {
            B_Measurement b_measurement = new B_Measurement(i, startNodeId, numberOfMeasurements);
            System.out.println(b_measurement.getAverageMeasurement());
        }
    }
}
