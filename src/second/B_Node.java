package second;

import java.util.ArrayList;

class B_Node {

    private ArrayList<B_Edge> neighbours;
    private boolean visited = false;
    final private String id;

    B_Node(String id) {
        this.id = id;
        this.neighbours = new ArrayList<>();
    }

    boolean isVisited() {
        return visited;
    }

    void setVisited(boolean visited) {
        this.visited = visited;
    }

    void addNeighbour(B_Edge edge) {
        if (containsNeighbour(edge)) {
            return;
        }
        this.neighbours.add(edge);
    }

    void removeNeighbour(int index) {
        this.neighbours.remove(index);
    }

    void removeNeighbour(B_Edge edge) {
        this.neighbours.remove(edge);
    }

    B_Edge getNeighbour(int index) {
        return this.neighbours.get(index);
    }

    boolean containsNeighbour(B_Edge edge) {
        return this.neighbours.contains(edge);
    }

    ArrayList<B_Edge> getNeighbours() {
        return new ArrayList<B_Edge>(this.neighbours);
    }

    String getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof B_Node)) {
            return false;
        }

        B_Node other = (B_Node) object;
        return this.getId().equals(other.getId());
    }

    @Override
    public String toString() {
        return id;
    }

}
