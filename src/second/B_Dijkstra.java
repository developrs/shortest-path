package second;

import java.util.*;

class B_Dijkstra {

    private B_Graph graph;
    private String startNodeId;
    private String targetNodeId;
    private HashMap<String, String> predecessors;
    private HashMap<String, Integer> distances;
    private PriorityQueue<B_Node> availableNodes;

    B_Dijkstra(B_Graph graph, String startNodeId, String targetNodeId) {

        this.graph = graph;
        Set<String> nodeKeys = this.graph.getNodeKeys();

        if (!nodeKeys.contains(startNodeId)) {
            throw new IllegalArgumentException("Der Graph enthält nicht den angegebenen Startknoten.");
        }

        this.startNodeId = startNodeId;
        this.targetNodeId = targetNodeId;
        this.predecessors = new HashMap<>();
        this.distances = new HashMap<>();
        this.availableNodes = new PriorityQueue<>(nodeKeys.size(), new Comparator<B_Node>() {
            public int compare(B_Node node1, B_Node node2) {
                int weight1 = B_Dijkstra.this.distances.get(node1.getId());
                int weight2 = B_Dijkstra.this.distances.get(node2.getId());
                return weight1 - weight2;
            }
        });

        for (String key : nodeKeys) {
            this.predecessors.put(key, null);
            this.distances.put(key, Integer.MAX_VALUE);
        }

        this.distances.put(startNodeId, 0);
        B_Node start = graph.getNode(startNodeId);

        ArrayList<B_Edge> sourceNeighbours = start.getNeighbours();

        for (B_Edge edge : sourceNeighbours) {
            B_Node neighbour = edge.getNeighbour(start);

            this.predecessors.put(neighbour.getId(), startNodeId);
            this.distances.put(neighbour.getId(), edge.getWeight());
            this.availableNodes.add(neighbour);
        }

        start.setVisited(true);
        executeDijkstra();
    }

    private void executeDijkstra() {

        while (this.availableNodes.size() > 0) {
            B_Node next = this.availableNodes.poll();

            int distanceToNext = this.distances.get(next.getId());
            ArrayList<B_Edge> nextNeighbours = next.getNeighbours();

            for (B_Edge e : nextNeighbours) {
                B_Node other = e.getNeighbour(next);
                if (!other.isVisited()) {
                    int currentWeight = this.distances.get(other.getId());
                    int newWeight = distanceToNext + e.getWeight();

                    if (newWeight < currentWeight) {
                        this.predecessors.put(other.getId(), next.getId());
                        this.distances.put(other.getId(), newWeight);
                        this.availableNodes.remove(other);
                        this.availableNodes.add(other);
                    }
                }
            }
            next.setVisited(true);
            if(next.equals(new B_Node(targetNodeId))) {
                break;
            }
        }
    }

    LinkedList<B_Node> getPath(String targetNodeId){
        LinkedList<B_Node> path = new LinkedList<>();
        path.add(graph.getNode(targetNodeId));

        while(!targetNodeId.equals(this.startNodeId)){
            B_Node predecessor = graph.getNode(this.predecessors.get(targetNodeId));
            targetNodeId = predecessor.getId();
            path.add(0, predecessor);
        }
        return path;
    }

}
