package second;

class B_Edge implements Comparable<B_Edge> {
    private final B_Node source;
    private final B_Node destination;
    private final int weight;

    B_Edge(B_Node source, B_Node destination, int weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    B_Node getDestination() {
        return destination;
    }

    B_Node getSource() {
        return source;
    }

    int getWeight() {
        return weight;
    }

    B_Node getNeighbour(B_Node node) {

        if (!node.equals(source) && !node.equals(destination)) {
            return null;
        }
        return (node.equals(source)) ? destination : source;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        B_Edge other = (B_Edge) obj;
        if (this.source.equals(other.getSource()) && this.destination.equals(other.getDestination())) {
            return true;
        } else if (this.source.equals(other.getDestination()) && this.destination.equals(other.getSource())) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(B_Edge other) {
        return this.weight - other.weight;
    }

    @Override
    public int hashCode() {
        return (source.getId() + destination.getId()).hashCode();
    }

    @Override
    public String toString() {
        return this.getSource() + " -> " + this.getDestination() + ": " + this.getWeight();
    }


}