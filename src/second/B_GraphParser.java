package second;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

class B_GraphParser {

    static B_Graph parse(String path) {

        BufferedReader bufferedReader = null;
        File file = new File(path);

        int counter = 0;
        int numberOfNodes = 0;
        int numberOfEdges = 0;
        B_Graph graph = new B_Graph();

        B_Edge[] edges = new B_Edge[numberOfEdges];

        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            String line;

            while (null != (line = bufferedReader.readLine())) {

                String[] parts = line.split("\\s");

                if (counter == 0) {
                    numberOfNodes = Integer.parseInt(parts[2]);
                    numberOfEdges = Integer.parseInt(parts[3]);
                    edges = new B_Edge[numberOfEdges];
                } else {
                    B_Node source = new B_Node(parts[1]);
                    B_Node destination = new B_Node(parts[2]);
                    int weight = Integer.parseInt(parts[3]);

                    edges[counter - 1] = new B_Edge(source, destination, weight);
                }
                counter++;
            }

            for(int i = 1; i <= numberOfNodes; i++) {
                graph.addNode(new B_Node(i + ""), true);
            }

            for (B_Edge edge : edges) {
                B_Node source = graph.getNode(edge.getSource().getId());
                B_Node destination = graph.getNode(edge.getDestination().getId());
                int weight = edge.getWeight();
                graph.addEdge(source, destination, weight);
            }

            return graph;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
