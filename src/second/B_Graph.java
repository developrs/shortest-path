package second;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class B_Graph {
    private final HashMap<String, B_Node> nodes;
    private final HashMap<Integer, B_Edge> edges;

    public B_Graph() {
        this.nodes = new HashMap<>();
        this.edges = new HashMap<>();
    }

    void resetVisitedNodes() {
        for(Map.Entry<String, B_Node> nodeSet : this.nodes.entrySet()) {
            nodeSet.getValue().setVisited(false);
        }
    }

    boolean addNode(B_Node node, boolean overwriteExisting) {
        B_Node current = this.nodes.get(node.getId());
        if (current != null) {
            if (!overwriteExisting) {
                return false;
            }
            while (current.getNeighbours().size() > 0) {
                this.removeEdge(current.getNeighbour(0));
            }
        }
        nodes.put(node.getId(), node);
        return true;
    }

    boolean addEdge(B_Node source, B_Node destination, int weight) {

        if (source.equals(destination)) {
            return false;
        }

        B_Edge edge = new B_Edge(source, destination, weight);

        if (this.containsEdge(edge)) {
            return false;
        } else if (source.containsNeighbour(edge) || destination.containsNeighbour(edge)) {
            return false;
        }

        int hash = edge.hashCode();
        edges.put(hash, edge);
        source.addNeighbour(edge);
        destination.addNeighbour(edge);
        return true;
    }

    B_Edge removeEdge(B_Edge edge) {
        edge.getSource().removeNeighbour(edge);
        edge.getDestination().removeNeighbour(edge);
        return this.edges.remove(edge.hashCode());
    }

    B_Node getNode(String id) {
        return this.nodes.get(id);
    }

    private boolean containsEdge(B_Edge edge) {
        return (edge.getSource() != null && edge.getDestination() != null) && this.edges.containsKey(edge.hashCode());
    }

    public HashMap<Integer, B_Edge> getEdges() {
        return edges;
    }

    Set<String> getNodeKeys() {
        return this.nodes.keySet();
    }

    void printEdges() {
        printEdges(this.edges);
    }

    private static void printEdges(HashMap<Integer, B_Edge> edges) {
        System.out.println("-----------------Edges:");
        for (B_Edge edge : edges.values()) {
            System.out.println(edge.toString());
        }
    }

    void printNodes() {
        printNodes(this.nodes);
    }

    private static void printNodes(HashMap<String, B_Node> nodes) {
        System.out.println("-----------------Nodes:");

        for (B_Node node : nodes.values()) {
            System.out.println(node.toString());
        }
    }

}
