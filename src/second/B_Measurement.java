package second;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class B_Measurement {

    private String filePath;
    private String startNodeId;
    private int numberOfMeasurements;

    private List<Long> measurements = new ArrayList<>();
    private long averageMeasurement = 0;

    public B_Measurement(String filePath, String startNodeId, int numberOfMeasurements) {
        this.filePath = filePath;
        this.startNodeId = startNodeId;
        this.numberOfMeasurements = numberOfMeasurements;

        B_Graph graph = B_GraphParser.parse(this.filePath);
        executeDijkstra(graph);
    }

    public B_Measurement(String filePath, String startNodeId, String targetNodeId, int numberOfMeasurements) {
        this.filePath = filePath;
        this.startNodeId = startNodeId;
        this.numberOfMeasurements = numberOfMeasurements;

        B_Graph graph = B_GraphParser.parse(this.filePath);
        executeDijkstra(graph, targetNodeId);
    }

    public B_Measurement(int numberOfNodes, String startNodeId, int numberOfMeasurements) {
        this.startNodeId = startNodeId;
        this.numberOfMeasurements = numberOfMeasurements;

        B_Graph graph = new B_GraphGenerator().generate(numberOfNodes);
        executeDijkstra(graph);
    }

    public B_Measurement(int numberOfNodes, int numberOfEdges, String startNodeId, String targetNodeId, int numberOfMeasurements) {
        this.startNodeId = startNodeId;
        this.numberOfMeasurements = numberOfMeasurements;

        B_Graph graph = new B_GraphGenerator().generate(numberOfNodes, numberOfEdges);
        executeDijkstra(graph, targetNodeId);
    }

    private void executeDijkstra(B_Graph graph) {
        for (int i = 0; i < this.numberOfMeasurements; i++) {
            System.gc();
            String targetNodeId = Math.max(1, Math.min(new Random().nextInt(graph.getNodeKeys().size() - 1), graph.getNodeKeys().size() - 1)) + "";
            long time = executeOneMeasurement(graph, targetNodeId);
            this.measurements.add(time);
            graph.resetVisitedNodes();
        }
        calcAvg();
    }

    private void executeDijkstra(B_Graph graph, String targetNodeId) {
        for (int i = 0; i < this.numberOfMeasurements; i++) {
            System.gc();
            long time = executeOneMeasurement(graph, targetNodeId);
            System.out.println(time);
            this.measurements.add(time);
            graph.resetVisitedNodes();
        }
        calcAvg();
    }

    private long executeOneMeasurement(B_Graph graph, String targetNodeId) {
        long start = System.currentTimeMillis();
        B_Dijkstra dijkstra = new B_Dijkstra(graph, this.startNodeId, targetNodeId);
        dijkstra.getPath(targetNodeId);
//            System.out.println(Arrays.toString(dijkstra.getPath(targetNodeId).toArray()));
        return System.currentTimeMillis() - start;
    }

    private void calcAvg() {
        long sum = 0;
        for (Long measurement : this.measurements) {
            sum += measurement;
        }
        this.averageMeasurement = sum / this.numberOfMeasurements;
    }

    public long getAverageMeasurement() {
        return averageMeasurement;
    }


}
