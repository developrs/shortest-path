package second;

import java.util.ArrayList;
import java.util.Random;

class B_GraphGenerator {

    private int numberOfNodes;

    B_Graph generate(int numberOfNodes) {
        return generate(numberOfNodes, numberOfNodes * 2);
    }

    B_Graph generate(int numberOfNodes, int numberOfEdges) {
        this.numberOfNodes = numberOfNodes;

        if (numberOfNodes <= 0) {
            throw new IllegalArgumentException("numberOfNodes must be bigger than 0");
        }

        B_Graph graph = new B_Graph();
        ArrayList<B_Edge> edges = new ArrayList<>();

        for (int i = 0; i < numberOfNodes; i++) {
            graph.addNode(new B_Node(i + ""), true);
        }

        Random random = new Random();

        edges.add(new B_Edge(new B_Node(0 + ""), new B_Node(1 + ""), random.nextInt(numberOfNodes * 3 + 1)));

        for (int i = 1; i < numberOfNodes - 1; i++) {

            B_Node source = new B_Node(i + "");
            B_Node destination = new B_Node(Math.min(i + 1, numberOfNodes - 1) + "");
            int weight = random.nextInt(numberOfNodes * 3 + 1);
            edges.add(new B_Edge(source, destination, weight));

            int bound = numberOfNodes - i - 1;
            int edgesInserted = 0;

            if (bound > numberOfNodes - numberOfNodes / 4) {
                while (edgesInserted < 2) {
                    destination = getRandomNode(bound, i);
                    weight = random.nextInt(numberOfNodes * 3 + 1);

                    if (!edges.contains(new B_Edge(source, destination, weight)) && !source.equals(destination)) {
                        edges.add(new B_Edge(source, destination, weight));
                        edgesInserted++;
                    }
                }
            }
        }

        while (edges.size() < numberOfEdges) {
            B_Node source = getRandomNode(numberOfNodes - 1, 0);
            int sourceId = Integer.parseInt(source.getId());

            B_Node destination = getRandomNode(numberOfNodes - sourceId - 1, sourceId);
            int weight = getRandomWeight();

            if (!edges.contains(new B_Edge(source, destination, weight)) && !source.equals(destination)) {
                edges.add(new B_Edge(source, destination, weight));
            }
        }

        for(B_Edge edge : edges) {
            graph.addEdge(graph.getNode(edge.getSource().getId()), graph.getNode(edge.getDestination().getId()), edge.getWeight());
        }

        return graph;
    }

    private B_Node getRandomNode(int bound, int counter) {
        Random random = new Random();
        return new B_Node(Math.min(counter + random.nextInt(bound), numberOfNodes - 1) + "");
    }

    private Integer getRandomWeight() {
        return new Random().nextInt(numberOfNodes * 3 + 1);
    }
}
