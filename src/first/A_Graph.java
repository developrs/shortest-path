package first;

import java.util.List;

class A_Graph {
    private final List<A_Node> nodes;
    private final List<A_Edge> edges;

    A_Graph(List<A_Node> nodes, List<A_Edge> edges) {
        this.nodes = nodes;
        this.edges = edges;
    }

    List<A_Node> getNodes() {
        return nodes;
    }

    List<A_Edge> getEdges() {
        return edges;
    }

    private void printEdges() {
        printEdges(this.edges);
    }

    static void printEdges(List<A_Edge> edges) {
        System.out.println("-----------------Edges:");

        for (A_Edge edge : edges) {
            System.out.println(edge.toString());
        }
    }

    private void printNodes() {
        printNodes(this.nodes);
    }

    static void printNodes(List<A_Node> nodes) {
        System.out.println("-----------------Nodes:");

        for (A_Node node : nodes) {
            System.out.println(node.toString());
        }
    }
}
