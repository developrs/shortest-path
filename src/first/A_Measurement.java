package first;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class A_Measurement {

    private String startNodeId;
    private int numberOfMeasurements;

    private List<Long> measurements = new ArrayList<>();
    private long averageMeasurement = 0;

    public A_Measurement(int numberOfNodes, String startNodeId, int numberOfMeasurements) {
        this.startNodeId = startNodeId;
        this.numberOfMeasurements = numberOfMeasurements;

        A_Graph graph = new A_GraphGenerator().generate(numberOfNodes);
        executeDijkstra(graph);
    }

    private void executeDijkstra(A_Graph graph) {
        for (int k = 0; k < this.numberOfMeasurements; k++) {
            String targetNodeId = Math.max(1, Math.min(new Random().nextInt(graph.getNodes().size() - 1), graph.getNodes().size() - 1)) + "";
            long start = System.nanoTime();
            A_Dijkstra dijkstra = new A_Dijkstra(graph, new A_Node(startNodeId));
            dijkstra.getPath(new A_Node(targetNodeId));
//            System.out.println(Arrays.toString(dijkstra.getPath(new A_Node(targetNodeId)).toArray()));
            long time = System.nanoTime() - start;
            this.measurements.add(time);
        }
        calcAvg();
    }

    private void calcAvg() {
        long sum = 0;
        for (Long measurement : this.measurements) {
            sum += measurement;
        }
        this.averageMeasurement = sum / this.numberOfMeasurements;
    }

    public long getAverageMeasurement() {
        return averageMeasurement;
    }


}
