package first;

class A_Edge {
    private final A_Node source;
    private final A_Node destination;
    private final int weight;

    A_Edge(A_Node source, A_Node destination, int weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    A_Node getDestination() {
        return destination;
    }

    A_Node getSource() {
        return source;
    }

    int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return this.getSource() + " -> " + this.getDestination() + ": " + this.getWeight();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        A_Edge other = (A_Edge) obj;
        if (this.source.equals(other.getSource()) && this.destination.equals(other.getDestination())) {
            return true;
        } else if (this.source.equals(other.getDestination()) && this.destination.equals(other.getSource())) {
            return true;
        }
        return false;
    }
}