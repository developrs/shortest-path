package first;

import java.util.*;

class A_Dijkstra {

    private final List<A_Node> nodes;
    private final List<A_Edge> edges;
    private Set<A_Node> settledNodes;
    private Set<A_Node> unvisitedNodes;
    private Map<A_Node, A_Node> predecessors;
    private Map<A_Node, Integer> distance;

    A_Dijkstra(A_Graph graph, A_Node startNode) {
        this.nodes = new ArrayList<>(graph.getNodes());
        this.edges = new ArrayList<>(graph.getEdges());

        execute(startNode);
    }

    private void execute(A_Node source) {
        settledNodes = new HashSet<>();
        unvisitedNodes = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        distance.put(source, 0);
        unvisitedNodes.add(source);
        while (unvisitedNodes.size() > 0) {
            A_Node node = getMinimum(unvisitedNodes);
            settledNodes.add(node);
            unvisitedNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(A_Node node) {
        List<A_Node> neighbours = getNeighbours(node);
        for (A_Node target : neighbours) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unvisitedNodes.add(target);
            }
        }

    }

    private int getDistance(A_Node node, A_Node target) {
        for (A_Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && edge.getDestination().equals(target)) {
                return edge.getWeight();
            }
        }
        return -1;
    }

    private List<A_Node> getNeighbours(A_Node node) {
        List<A_Node> neighbours = new ArrayList<>();
        for (A_Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && !isVisited(edge.getDestination())) {
                neighbours.add(edge.getDestination());
            }
        }
        return neighbours;
    }

    private A_Node getMinimum(Set<A_Node> nodes) {
        A_Node minimum = null;
        for (A_Node node : nodes) {
            if (minimum == null) {
                minimum = node;
            } else {
                if (getShortestDistance(node) < getShortestDistance(minimum)) {
                    minimum = node;
                }
            }
        }
        return minimum;
    }

    private boolean isVisited(A_Node node) {
        return settledNodes.contains(node);
    }

    private int getShortestDistance(A_Node destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    LinkedList<A_Node> getPath(A_Node targetNode) {
        LinkedList<A_Node> path = new LinkedList<>();
        A_Node step = targetNode;
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        Collections.reverse(path);
        return path;
    }

}