package first;

import java.util.ArrayList;
import java.util.Random;

class A_GraphGenerator {

    private int numberOfNodes;

    A_Graph generate(int numberOfNodes) {

        this.numberOfNodes = numberOfNodes;

        if (numberOfNodes <= 0) {
            throw new IllegalArgumentException("numberOfNodes must be bigger than 0");
        }

        ArrayList<A_Node> nodes = new ArrayList<>();
        ArrayList<A_Edge> edges = new ArrayList<>();

        for (int i = 0; i < numberOfNodes; i++) {
            nodes.add(new A_Node(i + ""));
        }

        Random random = new Random();

        edges.add(new A_Edge(new A_Node(0 + ""), new A_Node(1 + ""), getRandomWeight()));

        for (int i = 1; i < numberOfNodes - 1; i++) {

            A_Node source = new A_Node(i + "");
            A_Node destination = new A_Node(Math.min(i + 1, numberOfNodes - 1) + "");
            int weight = getRandomWeight();
            edges.add(new A_Edge(source, destination, weight));

            int bound = numberOfNodes - i - 1;
            int edgesInserted = 0;

            if (bound > numberOfNodes - numberOfNodes / 4) {
                while (edgesInserted < 2) {
                    destination = getRandomNode(bound, i);
                    weight = getRandomWeight();

                    if (!edges.contains(new A_Edge(source, destination, weight)) && !source.equals(destination)) {
                        edges.add(new A_Edge(source, destination, weight));
                        edgesInserted++;
                    }
                }
            }
        }

        while (edges.size() < nodes.size() * 2) {
            A_Node source = getRandomNode(numberOfNodes - 1, 0);
            int sourceId = Integer.parseInt(source.getId());

            A_Node destination = getRandomNode(numberOfNodes - sourceId - 1, sourceId);
            int weight = getRandomWeight();

            if (!edges.contains(new A_Edge(source, destination, weight)) && !source.equals(destination)) {
                edges.add(new A_Edge(source, destination, weight));
            }
        }

        return new A_Graph(nodes, edges);
    }

    private A_Node getRandomNode(int bound, int counter) {
        Random random = new Random();
        return new A_Node(Math.min(counter + random.nextInt(bound), numberOfNodes - 1) + "");
    }

    private Integer getRandomWeight() {
        return new Random().nextInt(numberOfNodes * 3 + 1);
    }
}
